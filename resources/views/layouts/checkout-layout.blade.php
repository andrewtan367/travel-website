<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>@yield('title')</title>
       
        {{-- Link CSS dll --}}
        @include('includes.style')
        @stack('addon-style')
    </head>

    <body>
        <!-- Navbar Section -->
        @include('includes.navbar-alternate')

        @yield('content')

        <!-- Footer -->
        @include('includes.footer')

        {{-- Script JS dll --}}
        @include('includes.script')
        @stack('addon-script')
    </body>
</html>
