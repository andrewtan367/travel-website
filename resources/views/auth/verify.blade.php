@extends('layouts.app')

@push('addon-style')
    <link
    href="https://unpkg.com/aos@2.3.1/dist/aos.css"
    rel="stylesheet"
    />
    <link rel="stylesheet" href={{asset("frontend/css/main.css")}} />
    <link rel="stylesheet" href={{asset("frontend/css/details.css")}} />
    <link rel="stylesheet" href={{asset("frontend/libraries/xzoom/dist/xzoom.css")}} />
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
