@extends('layouts.checkout-success')

@section('title')
    Checkout Sucess
@endsection

@push('addon-style')
    <link
    href="https://unpkg.com/aos@2.3.1/dist/aos.css"
    rel="stylesheet"
    />
    <link rel="stylesheet" href={{asset("frontend/css/main.css")}} />
    <link rel="stylesheet" href={{asset("frontend/css/checkout.css")}} />
    <link rel="stylesheet" href={{asset("frontend/css/checkout-success.css")}} />
@endpush

@section('content')
    <!-- Main Content -->
    <main>
        <div class="section-success d-flex align-items-center">
            <div class="col text-center">
                <img
                    src={{asset("frontend/img/success-image.png")}}
                    data-aos="flip-left"
                    data-aos-duration="800"
                />
                <div data-aos="flip-left" data-aos-duration="1000">
                    <h1>Yeay Sucess!</h1>
                    <p>
                        We've sent you email for trip instruction <br />
                        please read it as well
                    </p>
                    <a href={{asset("/")}} class="btn btn-homepage px-5"
                        >Homepage</a
                    >
                </div>
            </div>
        </div>
    </main>
@endsection

@push('addon-script')
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
@endpush