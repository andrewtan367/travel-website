@extends('layouts.app')

@section('title')
    NOMADS
@endsection

@push('addon-style')
    <link
    href="https://unpkg.com/aos@2.3.1/dist/aos.css"
    rel="stylesheet"
    />
    <link rel="stylesheet" href={{asset("frontend/css/main.css")}} />
@endpush

@section('content')
    <!-- Header Banner -->
    <header class="text-center">
        <h1 data-aos="fade-up" data-aos-duration="600">
            Explore The Beautiful World <br />
            As Easy One Click
        </h1>

        <p data-aos="fade-up" data-aos-duration="800" class="mt-3">
            You Will See Beautiful Moment You Never See
        </p>

        <a
            data-aos="fade-up"
            data-aos-duration="800"
            href="#popular"
            class="btn btn-get-started px-4 mt-4"
            >Get Started</a
        >
    </header>

    <!-- Main Content -->
    <main>
        <div class="container">
            <section
                class="section-stats row justify-content-center text-center"
                id="stats"
                data-aos="zoom-in"
                data-aos-duration="600"
            >
                <div class="col-6 col-sm-3 stats-detail">
                    <h2>20K</h2>
                    <p>Members</p>
                </div>

                <div class="col-6 col-sm-3 stats-detail">
                    <h2>12</h2>
                    <p>Countries</p>
                </div>

                <div class="col-6 col-sm-3 stats-detail">
                    <h2>3K</h2>
                    <p>Hotels</p>
                </div>

                <div class="col-6 col-sm-3 stats-detail">
                    <h2>72</h2>
                    <p>Partners</p>
                </div>
            </section>
        </div>

        <section class="section-popular" id="popular">
            <div class="container">
                <div class="row">
                    <div class="col text-center section-popular-heading">
                        <h2 data-aos="fade-up" data-aos-duration="600">
                            Wisata Populer
                        </h2>

                        <p data-aos="fade-up" data-aos-duration="600">
                            Something That You Never Try Before
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-popular-content" id="popular_content">
            <div class="container">
                <div class="row section-popular-travel justify-content-center">
                    @foreach ($items as $item)
                    <div
                        class="col-sm-6 col-md-6 col-lg-3"
                        data-aos="fade-up"
                        data-aos-duration="800"
                        data-aos-anchor-placement="top-bottom">

                        <div
                            class="card-travel grow text-center d-flex flex-column"
                            style="background:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('{{$item-> galleries -> count() ? Storage::url($item -> galleries -> first() -> image) : ''}}') no-repeat center/cover;">

                            <div class="travel-country">{{$item -> Location}}</div>

                            <div class="travel-location">{{$item -> title}}</div>

                            <div class="travel-button mt-auto">
                                <a href="{{route('detail', $item -> slug)}}" class="btn btn-travel-details">View Details</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="section-networks" id="networks">
            <div class="container">
                <div class="row">
                    <div
                        class="col-md-4"
                        data-aos="fade-up"
                        data-aos-duration="600"
                    >
                        <h2>Our Networks</h2>
                        <p>
                            Companies are trusted us more <br />
                            than just a trip
                        </p>
                    </div>
                    <div
                        class="col-md-8 text-center"
                        data-aos="fade-up"
                        data-aos-duration="600"
                    >
                        <img
                            src="/frontend/img/logo_partner.png"
                            alt="Networks Partner"
                            class="img-partner"
                        />
                    </div>
                </div>
            </div>
        </section>

        <section
            class="section-testimonial-heading"
            id="testimonial_heading"
        >
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h2 data-aos="fade-up" data-aos-duration="600">
                            They Are Loving Us
                        </h2>
                        <p data-aos="fade-up" data-aos-duration="600">
                            Moments were giving them <br />
                            the best experience.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section
            class="section-testimonial-content"
            id="testimonial_content"
        >
            <div class="container">
                <div
                    class="row section-popular-travel justify-content-center"
                >
                    <div
                        class="col-sm-6 col-md-6 col-lg-4"
                        data-aos="fade-up"
                        data-aos-duration="800"
                    >
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img
                                    src="/frontend/img/avatar1.png"
                                    alt="user"
                                    class="mb-4"
                                />
                                <h3>John Doe</h3>
                                <p class="testimonial">
                                    “ It was glorious and I could not stop
                                    to say wohooo for every single moment
                                    Dankeeeeee “
                                </p>
                                <hr />
                                <p class="trip-to">Trip To Ubud</p>
                            </div>
                        </div>
                    </div>

                    <div
                        class="col-sm-6 col-md-6 col-lg-4"
                        data-aos="fade-up"
                        data-aos-duration="800"
                    >
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img
                                    src="/frontend/img/avatar2.png"
                                    alt="user"
                                    class="mb-4"
                                />
                                <h3>Clarisse Meyer</h3>
                                <p class="testimonial">
                                    “ The trip was amazing and I saw
                                    something beautiful in that Island that
                                    makes me want to learn more “
                                </p>
                                <hr />
                                <p class="trip-to">Trip To Raja Ampat</p>
                            </div>
                        </div>
                    </div>

                    <div
                        class="col-sm-6 col-md-6 col-lg-4"
                        data-aos="fade-up"
                        data-aos-duration="800"
                    >
                        <div class="card card-testimonial text-center">
                            <div class="testimonial-content">
                                <img
                                    src="/frontend/img/avatar3.png"
                                    alt="user"
                                    class="mb-4"
                                />
                                <h3>Dwilita A</h3>
                                <p class="testimonial">
                                    “ I loved it when the waves was shaking
                                    harder — I was scared too “
                                </p>
                                <hr />
                                <p class="trip-to">Trip To Nusa Penida</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 text-center button-group"
                    data-aos="fade-up"
                    data-aos-duration="950">
                        <a href="#" class="btn btn-need-help px-4 mx-1 mt-2"
                            >I Need Help</a
                        >
                        <a
                            href="{{route('register')}}"
                            class="btn btn-get-started px-4 mx-1 mt-2"
                            >Get Started</a
                        >
                    </div>
                </div>
            </div>
        </section>
    </main>
    
@endsection

@push('addon-script')
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
@endpush