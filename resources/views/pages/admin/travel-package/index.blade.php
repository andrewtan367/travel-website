@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Travel Package</h1>
            <a href="{{route('travel-package.create')}}" class="btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-plus fa-sm text-white-50"></i>&nbsp;Add Travel Package</a> 
        </div>

        {{-- table --}}
        <div class="row">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" width="100%" cellspacing="0">
                        <thead>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Location</th>
                            <th>Type</th>
                            <th>Departure Date</th>
                            <th>Action</th>
                        </thead>

                        <tbody>
                            @forelse ($items as $item)
                                <tr>
                                    <td>{{$item -> id}}</td>
                                    <td>{{$item -> title}}</td>
                                    <td>{{$item -> Location}}</td>
                                    <td>{{$item -> type}}</td>
                                    <td>{{$item -> departure_date}}</td>
                                    <td>
                                        <a href="{{route('travel-package.edit', $item -> id)}}" class="btn btn-warning">
                                        <i class="fa fa-pencil-alt"></i></a>

                                        <form action="{{route('travel-package.destroy', $item -> id)}}" 
                                        method="POST" class="d-inline">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6" class="text-center">Data Kosong</td>
                                </tr>
                                
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

