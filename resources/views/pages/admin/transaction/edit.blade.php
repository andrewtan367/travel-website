@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Transaction Status</h1>
        </div>

        @if ($errors -> any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                    
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                </ul>
            </div>
            
        @endif

        <div class="card-shadow">
            <div class="card-body">
                <form action="{{route('transaction.update', $item->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="transaction_status">Status</label>
                    <select name="transaction_status" required class="form-control">
                        <option value="{{$item -> transaction_status}}">Don't Replace It ({{$item -> transaction_status}})</option>
                        <option value="IN_CART">In Cart</option>
                        <option value="PENDING">Pending</option>
                        <option value="SUCCESS">Success</option>
                        <option value="CANCEL">Cancel</option>
                        <option value="FAILED">Failed</option>
                    </select>
                </div>

                <button class="btn btn-primary btn-block" type="submit">Update Now</button>
                <a href="{{route('transaction.index')}}" class="btn btn-danger btn-block">Cancel</a>
            </form>
            </div>
        </div>

    </div>
@endsection

