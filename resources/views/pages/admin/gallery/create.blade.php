@extends('layouts.admin')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Gallery</h1>
        </div>

        @if ($errors -> any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                </ul>
            </div>
            
        @endif

        <div class="card-shadow">
            <div class="card-body">
                <form action="{{route('gallery.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="travel_packages_id">Travel Package</label>
                    <select name="travel_packages_id" required class="form-control">
                        <option value="">Choose The Travel Package</option>
                        @foreach ($travel_packages as $travel_package)
                            <option value="{{$travel_package -> id}}">{{$travel_package -> title}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="image">Images</label>
                    <input type="file" name="image" class="form-control">
                </div>

                <button class="btn btn-primary btn-block" type="submit">Save</button>
                <a href="{{route('gallery.index')}}" class="btn btn-danger btn-block">Cancel</a>
            </form>
            </div>
        </div>

    </div>
@endsection

