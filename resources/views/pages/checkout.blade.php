@extends('layouts.checkout-layout')

@section('title')
    Checkout
@endsection

@push('addon-style')
    <link
        href="https://unpkg.com/aos@2.3.1/dist/aos.css"
        rel="stylesheet"
    />
    <link
        rel="stylesheet"
        href={{asset("frontend/libraries/gijgo/css/gijgo.min.css")}}
    />
    <link rel="stylesheet" href={{asset("frontend/css/main.css")}} />
    <link rel="stylesheet" href={{asset("frontend/css/details.css")}} />
    <link rel="stylesheet" href={{asset("frontend/css/checkout.css")}} />
@endpush

@section('content')
    <!-- Main Content -->
    <main>
        <section class="section-details-heading"></section>
        <section class="section-details-content">
            <div class="container">
                <div class="row">
                    <div class="col p-0">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item active">
                                    Travel Package
                                </li>
                                <li class="breadcrumb-item active">
                                    Details
                                </li>
                                <li class="breadcrumb-item">Checkout</li>
                            </ol>
                        </nav>
                    </div>
                </div>

                <div class="row" data-aos="zoom-in" data-aos-duration="800">
                    <div class="col-lg-8 pl-lg-0">
                        <div class="card card-details">
                            @if ($errors -> any())
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach

                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    </ul>
                                </div>
                            @endif


                            <h1>Who is Going?</h1>
                            <p>Trip to {{$item->travel_package->title}}, {{$item->travel_package->Location}}</p>

                            <div class="attendee">
                                <table
                                    class="table table-borderless table-responsive-sm text-center"
                                >
                                    <thead>
                                        <tr>
                                            <td>Picture</td>
                                            <td>Name</td>
                                            <td>Nationality</td>
                                            <td>VISA</td>
                                            <td>Passport</td>
                                            <td></td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @forelse ($item->details as $detail)
                                            <tr>
                                                <td>
                                                    <img
                                                        src="https://ui-avatars.com/api/?name={{$detail->username}}"
                                                        height="60" class="rounded-circle"
                                                    />
                                                </td>
                                                <td class="align-middle">
                                                    {{$detail->username}}
                                                </td>
                                                <td class="align-middle">{{$detail->nationality}}</td>
                                                <td class="align-middle">
                                                    {{$detail->is_visa ? '30 Days' : 'N/A'}}
                                                </td>
                                                <td class="align-middle">
                                                    {{Carbon\Carbon::createFromDate($detail->doe_passport) > 
                                                    \Carbon\Carbon::now() ? 'Active' : 'Inactive'}}
                                                </td>
                                                <td class="align-middle">
                                                    <a href="{{route('checkout-remove', $detail->id)}}"><img src={{asset("frontend/img/icon-close.png")}} /></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6" class="text-center">No Visitor</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>

                            <div class="invite-member">
                                <h2>Add Member</h2>
                                <form class="form-inline" method="POST" action="{{route('checkout-create', $item->id)}}">
                                    @csrf
                                    <label for="username" class="sr-only">Name</label>
                                    <input
                                        type="text"
                                        name="username"
                                        class="form-control mb-2 mr-sm-2"
                                        id="username"
                                        placeholder="Username"
                                        required
                                    />

                                    <label for="nationality" class="sr-only">Nationality</label>
                                    <input
                                        type="text"
                                        name="nationality"
                                        class="form-control mb-2 mr-sm-2"
                                        style="width: 50px"
                                        id="nationality"
                                        placeholder="Natioanality"
                                        required
                                    />

                                    <label for="is_visa" class="sr-only">Visa</label>
                                    <select
                                        name="is_visa"
                                        id="is_visa"
                                        class="custom-select mb-2 mr-sm-2"
                                        required
                                    >
                                        <option value="" selected>
                                            VISA
                                        </option>
                                        <option value="1">
                                            30 Days
                                        </option>
                                        <option value="0">N/A</option>
                                    </select>

                                    <label for="doe_passport" class="sr-only">DOE Passport</label>
                                    <div class="input-group mb-2 mr-sm-2">
                                        <input
                                            type="text"
                                            name="doe_passport"
                                            class="form-control datepicker"
                                            id="doePassport"
                                            placeholder="DOE Passport"
                                            style="width: 140px"
                                            required
                                        />
                                    </div>

                                    <button type="submit" class="btn btn-add-now mb-2 px-4">
                                        Add Now
                                    </button>
                                </form>

                                <h3 class="disclaimer-heading">Note</h3>
                                <p class="disclaimer">
                                    You are only able invite member that has
                                    registered in www.nomads.co.id
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="card card-details card-right">
                            <h2>Checkout Information</h2>
                            <table class="trip-information">
                                <tr>
                                    <th width="50%">Members</th>
                                    <td width="50%" class="text-right">
                                        {{$item->details->count()}} Person
                                    </td>
                                </tr>

                                <tr>
                                    <th width="50%">Additional VISA</th>
                                    <td width="50%" class="text-right">
                                        ${{$item->additional_visa}}.00
                                    </td>
                                </tr>

                                <tr>
                                    <th width="50%">Trip Price</th>
                                    <td width="50%" class="text-right">
                                        ${{$item->travel_package->price}}.00 / Person
                                    </td>
                                </tr>

                                <tr>
                                    <th width="50%">Sub Total</th>
                                    <td width="50%" class="text-right">
                                        ${{$item->transaction_total}}.00
                                    </td>
                                </tr>

                                <tr>
                                    <th width="50%">
                                        Total (+Unique Code)
                                    </th>
                                    <td
                                        width="50%"
                                        class="text-right custom-text"
                                    >
                                        ${{$item->transaction_total}}.<span>{{mt_rand(0,99)}}</span>
                                    </td>
                                </tr>
                            </table>
                            <hr />
                            <h2>Payment Instruction</h2>
                            <p class="payment-instruction">
                                Please complete the payment to continue the
                                trip
                            </p>
                            <div class="bank">
                                <div class="bank-item pb-3">
                                    <img
                                        src={{asset("frontend/img/icon-bank.png")}}
                                        class="bank-image"
                                    />
                                    <div class="description">
                                        <h3>PT Nomads ID</h3>
                                        <p>0889-7805-4316</p>
                                        <p>Bank Central Asia</p>
                                    </div>
                                </div>

                                <div class="bank-item pb-3">
                                    <img
                                        src={{asset("frontend/img/icon-bank.png")}}
                                        class="bank-image"
                                    />
                                    <div class="description">
                                        <h3>PT Nomads ID</h3>
                                        <p>0881-5678-2297</p>
                                        <p>Bank HSBC</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="join-container">
                            <a
                                href={{route("checkout-success", $item->id)}}
                                class="btn btn-block btn-join-now py-2"
                                >I Have Made Payment</a
                            >
                        </div>

                        <div class="text-center mt-3">
                            <a
                                href={{route("detail", $item->travel_package->slug)}}
                                class="btn-cancel-booking"
                                >Cancel Booking</a
                            >
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@push('addon-script')
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src={{asset("/frontend/libraries/gijgo/js/gijgo.min.js")}}></script>
    <script>
        AOS.init();

        // gijgo
        $(document).ready(function() {
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                uiLibrary: "bootstrap4",
                icons: {
                rightIcon: '<img src={{asset("frontend/img/icon-calender.png")}} />',
            },
        });
        });
        
    </script>
@endpush