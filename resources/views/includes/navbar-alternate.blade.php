<!-- Navbar Section -->
<section class="container">
    <nav class="row navbar navbar-expand-lg navbar-light bg-white">
        <div class="navbar-nav mx-auto mr-sm-auto mr-lg-0 mr-md-auto">
            <a href={{route("home")}} class="navbar-brand">
                <img src={{asset("frontend/img/logo-baru@2x.png")}} alt="Logo NOMADS" />
            </a>
        </div>
        <ul class="navbar-nav mr-auto d-none d-lg-block">
            <li>
                <span> | &nbsp; Beyond The Explorer Of The World</span>
            </li>
        </ul>
    </nav>
</section>