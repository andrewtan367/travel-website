<section class="container">
    <nav class="row navbar navbar-expand-lg navbar-light bg-white">
        <a href={{route("home")}} class="navbar-brand"
            ><img src={{asset("frontend/img/logo-baru@2x.png")}} alt="Logo U.traveL"
        /></a>

        <button
            class="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ml-auto mr-3">
                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link active"
                        >Home
                        <span class="sr-only">(current page)</span></a
                    >
                </li>

                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link">Travel Package</a>
                </li>

                <li class="nav-item dropdown mx-md-2">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        id="navbarDrop"
                        role="button"
                        data-toggle="dropdown"
                    >
                        Services
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Visa</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">Passport</a>
                    </div>
                </li>

                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link">What They Say</a>
                </li>
            </ul>

            @guest
                <!-- Mobile Login Button -->
                <form class="form-inline d-md-block d-lg-none">
                    <button 
                        class="btn btn-login my-2 my-sm-0" type="button" 
                        onclick="event.preventDefault(); location.href='{{asset('login')}}';">
                        Login
                    </button>
                </form>

                <!-- Desktop Button -->
                <form class="form-inline my-2 my-lg-0 d-none d-lg-block">
                    <button
                        class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" 
                        onclick="event.preventDefault(); location.href='{{asset('login')}}';">
                        Login
                    </button>
                </form>
            @endguest


            @auth
                <!-- Mobile Login Button -->
                <form class="form-inline d-md-block d-lg-none" action="{{asset('logout')}}" method="POST">
                    @csrf
                    <button class="btn btn-login my-2 my-sm-0" type="submit">
                        Logout
                    </button>
                </form>

                <!-- Desktop Button -->
                <form class="form-inline my-2 my-lg-0 d-none d-lg-block" action="{{asset('logout')}}" method="POST">
                    @csrf
                    <button
                        class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="submit">
                        Logout
                    </button>
                </form>
            @endauth
        </div>
    </nav>
</section>