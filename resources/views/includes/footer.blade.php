<footer class="section-footer mb-4 border-top">
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="row">
                    <div class="col-6 col-lg-3">
                        <h5>Features</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Nomads For Corporate</a>
                            </li>
                            <li><a href="#">Nomads Affiliate</a></li>
                            <li><a href="#">Term & Condition</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>

                    <div class="col-6 col-lg-3">
                        <h5>Products</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Flights</a></li>
                            <li><a href="#">Hotels</a></li>
                            <li><a href="#">Visa</a></li>
                            <li><a href="#">Passport</a></li>
                        </ul>
                    </div>

                    <div class="col-6 col-lg-3">
                        <h5>Company</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Media</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Help Center</a></li>
                        </ul>
                    </div>

                    <div class="col-6 col-lg-3">
                        <h5>About Nomads</h5>
                        <ul class="list-unstyled">
                            <li><a href="#">How To Book</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Installment</a></li>
                            <li><a href="#">Term & Conditions</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div
            class="row border-top justify-content-center align-items-center"
        >
            <div class="copyright">
                Copyright © 2014-2020 U.traveL. All rights reserved.
            </div>
        </div>
    </div>
</footer>