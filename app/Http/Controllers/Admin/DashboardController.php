<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\TransactionModel;
use App\TravelPackageModel;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {

        return view ('pages.admin.dashboard', [
            'travel_package' => TravelPackageModel::count(),
            'transaction' => TransactionModel::count(),
            'transaction_pending' => TransactionModel::where('transaction_status', 'PENDING')->count(),
            'transaction_success' => TransactionModel::where('transaction_status', 'SUCCESS')->count()
        ]);
    }
}
