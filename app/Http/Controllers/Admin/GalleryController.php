<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\GalleryModel;
use App\Http\Requests\Admin\GalleryRequest;
use App\TravelPackageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ngambil data gallery dan detail travel_package
        $items = GalleryModel::with(['travel_package'])->get();
        return view('pages.admin.gallery.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $travel_packages = TravelPackageModel::all();
        return view('pages.admin.gallery.create', compact('travel_packages'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        $data = $request->all();
        $data['image'] =  $request->file('image')->store('assets/gallery', 'public');

        GalleryModel::create($data);
        return redirect()->route('gallery.index');
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = GalleryModel::findOrFail($id);
        $travel_packages = TravelPackageModel::all();

        return view('pages.admin.gallery.edit', compact('item', 'travel_packages'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryRequest $request, $id)
    {
        $data = $request->all();
        $data['image'] =  $request->file('image')->store('assets/gallery', 'public');

        $item = GalleryModel::findOrFail($id);
        $item->update($data);
        return redirect()->route('gallery.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = GalleryModel::findOrFail($id);
        $item->delete();

        return redirect()->route('gallery.index');
    }
}
