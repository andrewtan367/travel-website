<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TravelPackageModel;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $items = TravelPackageModel::with(['galleries'])->get();
        return view('pages.home', compact('items'));
    }
}
