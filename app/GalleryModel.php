<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GalleryModel extends Model
{
    protected $table = "galleries";

    use SoftDeletes;

    protected $fillable = ['travel_packages_id', 'image'];

    protected $hidden = [];

    public function travel_package() {
        return $this->belongsTo(TravelPackageModel::class, 'travel_packages_id', 'id');
    }
}
