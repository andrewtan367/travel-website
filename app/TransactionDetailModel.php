<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetailModel extends Model
{
    protected $table = "transaction_details";

    protected $fillable = [
        'transactions_id', 'username', 'nationality', 
        'is_visa', 'doe_passport'
    ];

    public function transaction() {
        return $this->belongsTo(TransactionModel::class, 'transactions_id', 'id');
    }
}
